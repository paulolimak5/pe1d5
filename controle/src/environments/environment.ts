// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyA6nv93xMnT5yNi-7zDCWlA_UdEzqXBdW0",
    authDomain: "controle-5a5d8.firebaseapp.com",
    projectId: "controle-5a5d8",
    storageBucket: "controle-5a5d8.appspot.com",
    messagingSenderId: "926392018091",
    appId: "1:926392018091:web:a6d12f913ac27ea9f983ee"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
